#!/bin/sh
#
# need to tweak some config options so "podman build" can run inside a docker container in gitlab ci
#

echo "# update & install packages"
dnf update -y
#dnf module -y enable container-tools:2.0
dnf install -y podman podman-docker buildah skopeo jq
dnf clean all -y

# storage: can't use overlay, docker does that already and it can't be nested.
echo "# update podman storage config"
test -f /etc/containers/storage.conf ||\
	cp -v /usr/share/containers/storage.conf /etc/containers
sed -i.orig -e '/^driver/s/overlay/vfs/' /etc/containers/storage.conf
diff -u /etc/containers/storage.conf.orig /etc/containers/storage.conf

# cgroups: there is no systemd managing the container, so use cgroupfs directly
if test -f /usr/share/containers/containers.conf; then
    file="containers.conf"
else
    file="libpod.conf"
fi
echo "# update podman cgroups config ($file)"
cp /usr/share/containers/${file} /etc/containers
sed -i.orig \
    -e '/cgroup_manager *=/s/^# *//' \
    -e '/^cgroup_manager/s/systemd/cgroupfs/' \
    -e '/netns *=/s/^# *//' \
    -e '/^netns/s/private/host/' \
    /etc/containers/${file}
diff -u /etc/containers/${file}.orig /etc/containers/${file}

echo "# silence docker emulation notification"
touch /etc/containers/nodocker

echo "# policy update (drop beta)"
cat /etc/containers/policy.json \
  | jq 'del(.transports.docker."registry.access.redhat.com/namespace")' \
  | jq 'del(.transports.docker."registry.access.redhat.com/ubi9-beta")' \
  | jq 'del(.transports.docker)' \
  | tee /etc/containers/policy.json.new
mv /etc/containers/policy.json.new /etc/containers/policy.json

exit 0
