DRMINFO_VERSION = 8-1
DRMINFO_SOURCE = drminfo-drminfo-$(DRMINFO_VERSION).tar.gz
DRMINFO_SITE = https://gitlab.com/kraxel/drminfo/-/archive/drminfo-$(DRMINFO_VERSION)
DRMINFO_LICENSE = GPL-3.0+
DRMINFO_INSTALL_STAGING = YES
DRMINFO_DEPENDENCIES = host-pkgconf libinput jpeg cairo libdrm mesa3d libepoxy

$(eval $(meson-package))
