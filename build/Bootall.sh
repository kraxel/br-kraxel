#!/bin/sh

match="${1-.}"
script="/home/kraxel/projects/br-kraxel/tests/boot.expect"

for item in $(ls */images/*.boot.sh | grep -e "$match"); do
    name1=$(basename $item .boot.sh)
    name2=$(dirname $(dirname $item))
    clear

    xterm-title "$name2: $name1"
    echo "#"
    echo "# $name2: $name1 ($item)"
    echo "#"

    cat $item
    echo "#"

    if $script $item; then
	echo "# OK"
    else
	echo "# FAILED ($name2: $name1)"
	exit 1
    fi

    sleep 5
done
